# Overview
This is a script to analyze object position based on frame difference.  
For now, png images are required to use this. Transcode videos into PNG frames before using. This will likely be addressed in a future revision.  
Command line options overview :   
--firstframe=n            First frame to analyze   
--lastframe=n              Last frame to analyze   
--helptext               Print this  
--conversionrate=n         Conversion rate of pixels to meters  
--threshold=n               Threshold for average pixel values(R+G+B) within blocks to be zeroed. Lower values will be set to 0, higher ones left alone. Use higher values if you are having issues with line deletions, use lower ones if actual data is being deleted.  
--blocksize=n               Block size for line deletion phase. Use lower values for higher accuracy and slower speed, however extremely low values may cause issues on poorly filmed video. 2n must be a common divisor of both the x and y resolution. This limitation may be addressed in the future  
--outputcsv=filename.csv     Output csv file. Will not be outputted if left as default.   
--outputstdout=Y/n      Output csv style format to stdout. Default is n  
--outputgraph=Y/n                Output the graph. Default is y  
  
# Example:   
./tracker.py --firstframe=70 --lastframe=95 --conversionrate=2800 --threshold=150 --blocksize=20 Trial_1/  

# Dependencies:
Python 3.0  
sympy  
numpy  
scipy  
opencv  
matplotlib(optional, use --graph=n to not use)  
