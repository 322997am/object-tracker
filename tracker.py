#!/usr/bin/env python3

import sys
import sympy
import numpy as np
import scipy
from scipy import ndimage
import cv2

minblock = 20 # radius of the blocks used for 
differential = True
angular = False
framerate = 60
path = "./" 
helptext = """
This is a script to analyze object position based on frame difference. 
For now, png images are required to use this. Transcode videos into PNG frames before using. This will likely be addressed in a future revision. 
Command line options overview : 
--firstframe=n          First frame to analyze 
--lastframe=n          Last frame to analyze 
--helptext              Print this
--conversionrate=n      Conversion rate of pixels to meters
--threshold=n           Threshold for average pixel values(R+G+B) within blocks to be zeroed. Lower values will be set to 0, higher ones left alone. Use higher values if you are having issues with line deletions, use lower ones if actual data is being deleted.
--blocksize=n           Block size for line deletion phase. Use lower values for higher accuracy and slower speed, however extremely low values may cause issues on poorly filmed video. 2n must be a common divisor of both the x and y resolution. This limitation may be addressed in the future
--outputcsv=filename.csv   Output csv file. Will not be outputted if left empty. 
--outputstdout=Y/n      Output csv style format to stdout. Default is n
--outputgraph=Y/n             Output the graph. Default is Y

Example: 
./tracker.py --firstframe=70 --lastframe=95 --conversionrate=2800 --threshold=150 --blocksize=20 Trial_1/"""
conversionrate = 1000
firstframe = 1
lastframe = 1000
threshold = 150
relative = True
outputgraph = True
outputcsv = False
outputstdout = False
outputfilename = ""

for argument in sys.argv:
    argument = str(argument)
    if(argument == "./tracker.py"):
        pass
    if("--blocksize=" in argument):
        minblock = int(argument.split("=")[1])
    elif("--help" in argument or "-h" in argument):
        print(helptext)
        sys.exit(0)
    elif("--conversionrate" in argument):
        conversionrate = int(argument.split("=")[1])
    elif("--firstframe" in argument):
        firstframe = int(argument.split("=")[1])
    elif("--lastframe" in argument):
        lastframe = int(argument.split("=")[1])    
    elif("--threshold" in argument):
        threshold = int(argument.split("=")[1])
    elif("--outputcsv" in argument):
        outputfilename = str(argument.split("=")[1])
        if(outputfilename[-4:] != ".csv"):
            outputfilename += ".csv"
        outputcsv = True
    elif("--outputgraph" in argument):
        if("--outputgraph=y" in argument.lower()):
            outputgraph = True
        elif("--outputgraph=n" in argument.lower()):
            outputgraph = False
    elif("--outputstdout" in argument):
        if("--outputstdout=y" in argument.lower()):
            outputstdout = True
        elif("--outputstdout=n" in argument.lower()):
            outputstdout = False 
            
            
    else:
        path = argument

if(outputgraph == True):
    from matplotlib import pyplot as plt


if(path[-1] != "/"):
    path += "/"


xvalues = np.zeros(lastframe - firstframe)
yvalues = np.zeros(lastframe - firstframe)
initialimage = cv2.imread(path + str(firstframe).zfill(3) + ".png", cv2.IMREAD_COLOR)
if initialimage is None:
    print("No images found in path. Please ensure that they are png")
    sys.exit(1)
xresolution = initialimage.shape[1]
yresolution = initialimage.shape[0]
initblocksize = np.gcd(xresolution, yresolution)
if(initblocksize % (minblock * 2) != 0):
    print("block size is not a factor of the GCD of the x and y resolutions. This behaviour is unsupported")
    sys.exit(2)
for i in range(firstframe, lastframe):
    thisframe = cv2.imread(path + str(i).zfill(3) + ".png", cv2.IMREAD_COLOR)
    nextframe = cv2.imread(path + str(i + 1).zfill(3) + ".png", cv2.IMREAD_COLOR)
    framedifference = np.subtract(nextframe, thisframe)
    framedifference = np.absolute(framedifference)
    framedifference[framedifference > 127] = 0
    framedifference[framedifference > 60] = 255
    framedifference = np.sum(framedifference, axis=2)
    framedifference[framedifference < 127] = 0

    for x in range(minblock, xresolution, minblock * 2):
        for y in range(minblock, yresolution, minblock * 2):
            if(np.average(framedifference[y-minblock:y+minblock, x-minblock:x+minblock]) < threshold):
                framedifference[y-minblock:y+minblock, x-minblock:x+minblock] = 0
                pass
    position = scipy.ndimage.center_of_mass(framedifference)
    xpos = position[1] / conversionrate
    ypos = yresolution - position[0] / conversionrate
    xvalues[i - firstframe] = xpos
    yvalues[i - firstframe] = ypos

xvalues = np.subtract(xvalues, xvalues[0])
yvalues = np.subtract(yvalues, yvalues[0])

texttowrite = ""
for i in range(0, xvalues.shape[0]):
    texttowrite += str(xvalues[i]) + "," + str(yvalues[i]) + "\n"
if(outputcsv == True):
    f = open(outputfilename, "a")
    f.write(texttowrite)
    f.close()
if(outputstdout == True):
    print(texttowrite)
if(outputgraph == True):    
    plt.scatter(xvalues, yvalues)
    plt.title('Object position')
    plt.show()
    
            
            
        
    
